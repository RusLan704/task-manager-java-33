package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User persist(@NotNull User userDTO);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findById(@NotNull final String id);

    @NotNull
    List<User> findAll();

    @Nullable
    User removeByLogin(@NotNull String login);

    @Nullable
    User removeById(@NotNull String id);

    @NotNull
    List<User> removeAll();

}
