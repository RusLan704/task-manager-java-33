package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bakhtiyarov.tm.api.endpoint.ISessionEndpoint;
import ru.bakhtiyarov.tm.api.service.IPropertyService;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.converter.ISessionConverter;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionConverter sessionConverter;

    @NotNull
    @Autowired
    public SessionEndpoint(
            @NotNull final IPropertyService propertyService,
            @NotNull final ISessionConverter sessionConverter
    ) {
        this.propertyService = propertyService;
        this.sessionConverter = sessionConverter;
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        @Nullable final Session session = sessionService.open(login, password);
        return sessionConverter.toDTO(session);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void closeSession(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        sessionService.close(sessionConverter.toEntity(sessionDTO));
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean closeSessionAll(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        return sessionService.closeAll(session);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Integer getServerPort(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        return propertyService.getServerPort();
    }

}