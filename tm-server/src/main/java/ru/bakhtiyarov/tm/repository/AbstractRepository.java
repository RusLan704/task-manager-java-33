package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void addAll(@NotNull List<E> records) {
        records.forEach(this::merge);
    }

    @NotNull
    @Override
    public E merge(@NotNull final E e) {
        entityManager.merge(e);
        return e;
    }

    @NotNull
    @Override
    public E persist(@NotNull E record) {
        entityManager.persist(record);
        return record;
    }

    @Nullable
    @Override
    public E remove(@NotNull E record) {
        entityManager.remove(record);
        return record;
    }

}