package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyLastNameException extends AbstractException {

    @NotNull
    public EmptyLastNameException() {
        super("Error! Last name is empty...");
    }

}