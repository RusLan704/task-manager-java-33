package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidUserException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;

import java.util.List;

@Service
public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    @Autowired
    public ProjectService(
            @NotNull final IUserService userService,
            @NotNull final IProjectRepository projectRepository
    ) {
        this.userService = userService;
        this.projectRepository = projectRepository;
    }


    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable User user = userService.findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = new Project(name);
        project.setUser(user);
        persist(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable User user = userService.findById(userId);
        if (user == null) throw new InvalidUserException();
        @NotNull Project project = new Project(name, description);
        project.setUser(user);
        persist(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull IProjectRepository projectRepository = getRepository();
        projectRepository.removeAll(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull IProjectRepository projectRepository = getRepository();
        Project project = new Project();
        project = projectRepository.removeOneByIndex(userId, index);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull IProjectRepository projectRepository = getRepository();
        return projectRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull IProjectRepository projectRepository = getRepository();
        return projectRepository.findAllByUserId();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IProjectRepository projectRepository = getRepository();
        @Nullable Project project = projectRepository.removeOneById(userId, id);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull IProjectRepository projectRepository = getRepository();
        @Nullable Project project = projectRepository.removeOneByName(userId, name);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public List<Project> removeAll() {
        @NotNull IProjectRepository projectRepository = getRepository();
        @NotNull List<Project> projects = projectRepository.removeAll();
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return merge(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IProjectRepository projectRepository = getRepository();
        return projectRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return merge(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull IProjectRepository projectRepository = getRepository();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull IProjectRepository projectRepository = getRepository();
        return projectRepository.findOneByName(userId, name);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository() {
        return projectRepository;
    }

}