package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.api.service.IService;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity, R extends IRepository<E>> implements IService<E> {

    @NotNull
    protected abstract R getRepository();

    @Override
    public void addAll(@Nullable List<E> records) {
        if (records == null) return;
        @NotNull IRepository<E> repository = getRepository();
        repository.addAll(records);
    }

    @Override
    @Nullable
    public E persist(@Nullable final E record) {
        if (record == null) return null;
        @NotNull IRepository<E> repository = getRepository();
        return repository.persist(record);
    }

    @Override
    @Nullable
    public E merge(@Nullable final E record) {
        if (record == null) return null;
        @NotNull IRepository<E> repository = getRepository();
        @Nullable E newRecord = repository.merge(record);
        return newRecord;
    }

}