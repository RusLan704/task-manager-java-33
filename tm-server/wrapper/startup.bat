@If Not Exist "..\log" (
mkdir ..\log
)

@echo TASK MANAGER SERVER IS RUNNING...
@java -jar ./tm-server.jar > ../log/tm-server.log 2>&1
