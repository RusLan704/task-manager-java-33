package ru.bakhtiyarov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.SessionEndpoint;

@Component
public final class LogoutListener extends AbstractListener {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout user in program";
    }

    @EventListener(condition = "@logoutListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        sessionEndpoint.closeSessionAll(sessionService.getSession());
        sessionService.clearSession();
        System.out.println("[OK]");
    }

}
